import sqlite3
from flask import current_app, g
from flask.cli import with_appcontext
import click


def _dict_factory(cursor, row):
    data = {}
    for idx, col in enumerate(cursor.description):
        data[col[0]] = row[idx]

    return data


def _get_connection():
    if 'con' not in g:
        g.con = sqlite3.connect(
            current_app.config['DB_CON_STRING'], detect_types=sqlite3.PARSE_DECLTYPES
        )
        g.con.row_factory = _dict_factory

    return g.con


def _close_connection(e=None):
    con = g.pop('con', None)

    if con is not None:
        con.close()


def execute(query, param, commit=True):
    cur = _get_connection().cursor()
    row_id = cur.execute(query, param).lastrowid
    if commit:
        _get_connection().commit()
    else:
        _get_connection().rollback()
        row_id = -1

    cur.close()
    return dict(id=row_id)


def fetchall(query, params):
    cur = _get_connection().cursor()
    res = cur.execute(query, params).fetchall()
    cur.close()

    return res


def _init_db():
    con = _get_connection()

    with current_app.open_resource('base/schema.sql') as f:
        con.executescript(f.read().decode('utf-8'))


@click.command('init-db')
@with_appcontext
def initialize_db_command():
    _init_db()
    click.echo('Database initialized ...')


def init_app(app):
    app.teardown_appcontext(_close_connection)
    app.cli.add_command(initialize_db_command)
