import pika
from flask import current_app


class RabbitMQConfig:

    def __init__(self, host='localhost', exchange='item-exchange', queue='item-queue', routing_key='item'):
        self.host = host
        self.exchange = exchange
        self.queue = queue
        self.routing_queue = routing_key


class Producer:

    def __init__(self, server):
        self._server = server

    def publish_message(self, message):
        with pika.BlockingConnection(pika.ConnectionParameters(host=self._server.host)) as con:
            channel = con.channel()
            # Direct Exchange
            channel.exchange_declare(exchange=self._server.exchange, exchange_type='direct')

            channel.queue_declare(queue=self._server.queue)
            channel.basic_publish(
                exchange=self._server.exchange,
                routing_key=self._server.routing_queue,
                body=message
            )
            return message
