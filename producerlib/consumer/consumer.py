import pika
import json

from producerlib.query import sql_query
from producerlib.consumer.database import Database


class RabbitMQConfig:

    def __init__(self, host='localhost', exchange='item-exchange', queue='item-queue', routing_key='item'):
        self.host = host
        self.exchange = exchange
        self.queue = queue
        self.routing_key = routing_key


class Consumer:
    def __init__(self, server, db_uri):
        self._server = server
        self.db_uri = db_uri

    def complete_task(self, ch, method, properties, body):
        # Update status from 'pending' to 'completed' in Database.
        db = Database(self.db_uri)
        body = json.loads(body)
        response = db.execute(sql_query.UPDATE_ITEM, (body['id'],))

        print(':> Received %r' % body)

        # Acknowledgement: mark message as READ
        ch.basic_ack(delivery_tag=method.delivery_tag)

    def consume_message(self):
        con = pika.BlockingConnection(pika.ConnectionParameters(host=self._server.host))
        channel = con.channel()
        # Direct Exchange
        channel.exchange_declare(exchange=self._server.exchange, exchange_type='direct')

        # Get message from queue by by its routing key.
        channel.queue_declare(queue=self._server.queue)
        channel.queue_bind(
            exchange=self._server.exchange,
            queue=self._server.queue,
            routing_key=self._server.routing_key
        )

        # Consume from the queue and perform task on based on message.
        channel.basic_consume(
            queue=self._server.queue,
            on_message_callback=self.complete_task,
        )

        # Fetch message only when the current task is finished.
        channel.basic_qos(prefetch_count=1)
        print('[*] Waiting for messages. To exit press CTRL+C')

        # Start consuming
        channel.start_consuming()


if __name__ == '__main__':
    mq_server = RabbitMQConfig(
        'localhost',
        exchange='item-exchange',
        queue='item-queue',
        routing_key='item'
    )
    consumer = Consumer(mq_server, 'C:/Users/camre/Desktop/PyWork/ProducerConsumer/instance/queues.sqlite')
    consumer.consume_message()

