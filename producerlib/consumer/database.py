import sqlite3


class Database:

    def __init__(self, uri):
        self.con = None
        self.uri = uri

    @staticmethod
    def _dict_factory(cursor, row):
        data = {}
        for idx, col in enumerate(cursor.description):
            data[col[0]] = row[idx]

        return data

    def _get_connection(self):
        if self.con is None:
            self.con = sqlite3.connect(self.uri, detect_types=sqlite3.PARSE_DECLTYPES)
            self.con.row_factory = self._dict_factory

        return self.con

    def _close_connection(self):
        if self.con is not None:
            self.con.close()
            self.con = None

    def execute(self, query, param, commit=True):
        cur = self._get_connection().cursor()
        row_id = cur.execute(query, param).lastrowid
        if commit:
            self._get_connection().commit()
        else:
            self._get_connection().rollback()
            row_id = -1

        cur.close()
        return dict(id=row_id)

    def fetchall(self, query, params):
        cur = self._get_connection().cursor()
        res = cur.execute(query, params).fetchall()
        cur.close()
        return res
