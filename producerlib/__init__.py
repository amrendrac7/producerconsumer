import os
from flask import Flask


def create_app(config_file=None):
    app = Flask(__name__, instance_relative_config=True)
    app.config.from_mapping(
        SECRET_KEY='dev',
        DB_CON_STRING=os.path.join(app.instance_path, 'queues.sqlite')
    )

    if config_file is None:
        app.config.from_json('config.json', silent=True)
    else:
        app.config.from_mapping(config_file)

    try:
        os.makedirs(app.instance_path)
    except OSError:
        pass

    from .base.database import init_app
    from . import app as api

    init_app(app)

    app.register_blueprint(api.api_bp)

    return app
