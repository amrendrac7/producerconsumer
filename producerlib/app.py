from flask import Blueprint, request
from producerlib.service import producer_service

api_bp = Blueprint('api', __name__, url_prefix='/api')


@api_bp.route('/item', methods=['GET', 'POST'])
def item():
    return producer_service.add_item(request.method, request.json)
