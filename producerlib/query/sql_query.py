INSERT_ITEM = """
INSERT INTO items (item, status) VALUES (?, ?)    
"""

UPDATE_ITEM = """
UPDATE items set status='completed' WHERE id = ?
"""

FETCH_ITEMS = """
SELECT * FROM items
"""