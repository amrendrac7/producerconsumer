from flask import jsonify, make_response, current_app
import json

from producerlib.base import database
from producerlib.query import sql_query
from producerlib.base.producer import RabbitMQConfig, Producer
from producerlib.util import utility


def add_item(request, payload):
    try:
        if request == 'POST':
            if payload is None or not ('item' in payload):
                return make_response(jsonify({'error': "Invalid content-type or 'item' is missing"}), 400)
            param = (payload['item'], 'pending')
            response = database.execute(sql_query.INSERT_ITEM, param)
            server = RabbitMQConfig(
                current_app.config['MQ']['URI'],
                exchange=current_app.config['MQ']['EXCHANGE'],
                queue=current_app.config['MQ']['QUEUE'],
                routing_key=current_app.config['MQ']['ROUTING_KEY']
            )
            producer = Producer(server)
            producer.publish_message(json.dumps(response))
            return jsonify(response), 202
        elif request == 'GET':
            response = database.fetchall(sql_query.FETCH_ITEMS, ())
            return jsonify(response), 200
    except Exception as ex:
        return make_response(jsonify(utility.wrap_error(ex)), 500)
