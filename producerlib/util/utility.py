import traceback
import sys


def wrap_error(ex):
    ex_type, ex_value, ex_tb = sys.exc_info()
    trace = traceback.format_exception(ex_type, ex_value, ex_tb)
    return {'type': str(type(ex)), 'args': ex.args, 'stack': trace}
