# Producer-Consumer | RabbitMQ 

## Installation
Use the package manager pip to install dependencies in requirements.txt
```bash
pip install requirements.txt
```

## Usage
* Go to project directory in command prompt 
 ```bash
cd C:/Users/camre/Desktop/PyWork/ProducerConsumer
```
* Set properties and initialize DB
```bash
(venv) .../ProducerConsumer> SET FLASK_APP=producerlib
(venv) .../ProducerConsumer> SET FLASK_ENV=Development
(venv) .../ProducerConsumer> flask init-db
```
'instance' directory will be created in project folder.
Add config.json file to 'instance' directory with fields as following

##### /instance/config.json
```json
{
  "MQ": {
    "URI": "localhost",
    "EXCHANGE": "item-exchange",
    "QUEUE": "item-queue",
    "ROUTING_KEY": "item"
  }
}
```
* Run Flask (Producer)
```bash
(venv) .../ProducerConsumer> flask run --port=8080
``` 

Ensure following properties in consumer __main__ call:
1. DB_URI, path of the sqlite DB in instance directory
2. server host, same as provided to producer in config.json
3. exchange name, same as provided to producer in config.json
4. queue name, same as provided to producer in config.json
5. routing key, same as provided to producer in config.json

* Run consumer.py (Consumer)
```bash
(venv) .../ProducerConsumer/producerlib/consumer> python.py consumer.py
```

#### Calling API
* HTTPIE
```bash
$ http POST http://127.0.0.1:8080/api/item item=book 
```
* CURL
```bash
$ curl -H "Content-Type: application/json" -X POST -d "{\"item\": \"book\"}" http://127.0.0.1:8080/api/item
```
